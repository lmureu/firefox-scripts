// ==UserScript==
// @name 			Jango.com Player Font Fix
// @namespace 			tag:mureulor@gmail.com,2013-08:firefox-scripts/jango-font.user.js
// @downloadURL 		https://github.com/mooow/firefox-scripts/raw/master/jango-font.user.js
// @version 			0.1
// @description 		Make jango.com player's infobox use a smaller font
// @author 			Lorenzo Mureu
// @include 			http*://www.jango.com/*
// @run-at			document-end
// @grant 			GM_addStyle
// ==/UserScript==

GM_addStyle("div#player_info { font-size-adjust: 0.39 !important ; }")
