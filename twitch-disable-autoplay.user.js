// ==UserScript==
// @name                Disable autoplay on Twitch homepage
// @namespace           tag:mureulor@gmail.com:firefox-scripts/twitch-disable-autoplay.user.js
// @downloadURL         https://gitlab.com/lmureu/firefox-scripts/raw/master/twitch-disable-autoplay.user.js
// @version             0.3
// @include             https://www.twitch.tv/
// @include             http://www.twitch.tv/
// @description         Disable auto-play on Twitch's homepage
// @author              Lorenzo Mureu
// @run-at              document-idle
// @grant               none
// ==/UserScript==

for (const obj of document.getElementsByTagName("video")) {
    console.log("Deleting videos");
    obj.remove();
}

/**
 * NB: hide the carousel instead of deleting to prevent twitch from simply reloading it 
 */
for (const obj of document.getElementsByClassName("front-page-carousel")) {
    console.log("Deleting carousel");
    obj.hidden = true;
}
