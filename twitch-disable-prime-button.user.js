// ==UserScript==
// @name                Disable Prime Button on Twitch
// @namespace           tag:mureulor@gmail.com:firefox-scripts/twitch-disable-prime-button.user.js
// @downloadURL         https://gitlab.com/lmureu/firefox-scripts/raw/master/twitch-disable-prime-button.user.js
// @version             0.1a
// @include             https://www.twitch.tv/*
// @include             http://www.twitch.tv/*
// @description         Disable Prime Button on Twitch
// @author              Lorenzo Mureu
// @run-at              document-idle
// @grant               none
// ==/UserScript==

for(const obj of document.getElementsByClassName("top-nav__prime"))
{
    obj.remove();
}
