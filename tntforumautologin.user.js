// ==UserScript==
// @name 			TNTForum autologin
// @namespace 			tag:mureulor@gmail.com,2013-08:firefox-scripts/tntforumautologin.user.js
// @downloadURL 		https://github.com/mooow/firefox-scripts/raw/master/tntforumautologin.user.js
// @version 			0.2.2+
// @description 		Automatically performs login on forum.tntvillage.scambioetico.org
// @author 			Lorenzo Mureu
// @include 			http://forum.tntvillage.scambioetico.org/*
// @grant 			GM_getValue
// @grant 			GM_setValue
// @run-at			document-end
// ==/UserScript==

console.log("-1");

function hidebox()
{
	document.getElementById("tntalcp").remove();
}

function save() {
	div = document.getElementById("tntalcp");

	user = document.getElementById("tntalcp_user");
	pass = document.getElementById("tntalcp_pass");
	
	console.log(user.value+":"+pass.value);
	GM_setValue("userval", user.value);
	GM_setValue("passval", pass.value);

	p = document.createElement("p");
	p.textContent = "Saved";
	div.appendChild(p);
}

function showbox()
{
	console.log("1");
	div = document.createElement("div");
	div.id="tntalcp";
	console.log("2");
	div.style.top = 0;
	div.style.left = 0;
	div.style.width = "100vw";
	div.style.height = "100vh";
	//div.textContent = "Ciao ";
	div.style.position = "fixed";
	div.style.setProperty("background-color", "blue")
	console.log("3");

	h1   = document.createElement("h1");
	h1.textContent = "TNT Forum Autologin Control Panel";
	
	p    = document.createElement("p");
	p.textContent  = "Please save your username and password";

	user = document.createElement("input");
	user.id = "tntalcp_user";
	pass = document.createElement("input");
	pass.id = "tntalcp_pass";
	bttn = document.createElement("button");
	bttn.textContent = "OK";
	bttn.addEventListener("click", save, true);

		
	a = document.createElement("a");
	a.addEventListener("click", hidebox, true);
	a.textContent = "Hide TNT Forum AutoLogin Control Panel";

	div.appendChild(a);
	div.appendChild(h1);
	div.appendChild(p);
	div.appendChild(user);
	div.appendChild(pass);
	div.appendChild(bttn);
	document.body.appendChild(div);
}

console.log("0");
if ( document.body.textContent.contains("Connessione 'veloce'") ) {
	console.log("Querying userval and passval");
	userval = GM_getValue("userval");
	passval = GM_getValue("passval");
	if( passval == undefined || userval == undefined ) {
		console.log("settings box is needed");
		showbox();
	}
	console.log(userval+":"+passval);
	form = document.getElementsByTagName("form")[0];
	userin = document.getElementsByName("UserName")[0];
	passin = document.getElementsByName("PassWord")[0];

	console.log("Setting values");
	userin.value = userval;
	passin.value = passval;

	console.log("Submitting login form");
	form.submit();
}


a = document.createElement("a");
a.addEventListener("click", showbox, true);
a.textContent = "TNT Forum AutoLogin Control Panel";
document.body.appendChild(a);
