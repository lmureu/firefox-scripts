// ==UserScript==
// @name          Exhentai image resizer
// @namespace     tag:mureulor@gmail.com,2020:firefox-scripts/exhentai-image-resizer.user.js
// @downloadURL   https://gitlab.com/lmureu/firefox-scripts/raw/master/exhentai-image-resizer.user.js
// @description   Resize ex-hentai images so that they fit on a single window
// @include       https://exhentai.org/s/*
// @author        Lorenzo Mureu
// @version       1.5
// @run-at 	      document-idle
// @grant         none
// ==/UserScript==

function addGlobalStyle(css) {
    var head, style;
    head = document.getElementsByTagName('head')[0];
    if (!head) { return; }
    style = document.createElement('style');
    style.type = 'text/css';
    style.innerHTML = css;
    head.appendChild(style);
}

addGlobalStyle(`#img {
	max-height: 80vh !important;
	object-fit: contain;
	max-width: 100% !important;
}`);
