// ==UserScript==
// @name 			Facebook Chronological Order
// @namespace 			tag:mureulor@gmail.com,2013-08:firefox-scripts/facebookchronorder.user.js
// @downloadURL 		https://github.com/mooow/firefox-scripts/raw/master/facebookchronorder.user.js
// @version 			0.2
// @description 		Facebook's timeline posts will be sorted in chronological order (and not by "trend")
// @author 			Lorenzo Mureu
// @include 			https://www.facebook.com/*
// @grant 			none
// @run-at			document-start
// ==/UserScript==

x = "sk=h_chr"
console.log("search="+document.location.search);
if(document.location.pathname == "/" and !document.location.search.contains(x)) {
	if(document.location.search == "") {
		console.log("?+x");
		document.location.search = "?"+x;
	} else {
		console.log("&+x");
		document.location.search += "&"+x;
	}
}
