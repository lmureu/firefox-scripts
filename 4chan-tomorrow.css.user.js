// ==UserScript==
// @name        Tomorrow CSS for 4chan
// @namespace   tag:mureulor@gmail.com,2013-08:firefox-scripts/4chan-tomorrow.css.user.js
// @downloadURL https://github.com/mooow/firefox-scripts/raw/master/4chan-tomorrow.css.user.js
// @description Tomorrow CSS for 4chan
// @include     http*://*.4chan.org/*
// @version     1.01
// @grant       none
// @author      Lorenzo Mureu
// ==/UserScript==

document.selectedStyleSheetSet="Tomorrow"
