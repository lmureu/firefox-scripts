// ==UserScript==
// @name                Add cookies to exhentai
// @namespace           tag:mureulor@gmail.com,2020:firefox-scripts/exhentai.user.js
// @version             0.2.1
// @description         Add cookies to exhentai
// @author              Lorenzo Mureu
// @include             https://exhentai.org/
// @run-at              document-idle
// @grant               none
// ==/UserScript==


function getCookie(target) {
    target = decodeURIComponent(target.trim().toLowerCase());

    if (document.cookie == undefined) return undefined;

    for (const cookie of document.cookie?.split(';')) {
        const pair = cookie.split('=');
        const key = decodeURIComponent(pair[0].trim().toLowerCase());
        const value = decodeURIComponent(pair[1].trim().toLowerCase());

        if (target == key) {
            console.log("Stored cookie:", key, value);
            return value;
        }
    }

    return undefined;
}

function setCookie(key, value) {
    var expires = Math.floor(Date.now() / 1000) + 259200000;
    console.log("Setting cookie:", key, value, expires);
    document.cookie = `${key}=${value};expires=${expires};secure;sameSite=strict`;
}


(function () {
    // TODO: EDIT HERE --------------------------
    console.log("You forgot to put your data!");
    const data = {
        "ipb_member_id": "",
        "ipb_pass_hash": ""
    };
    // ------------------------------------------

    var dirty = false;

    for (const [key, value] of Object.entries(data)) {
        if (getCookie(key) != value) {
            setCookie(key, value);
            dirty = true;
        }
    }

    if (dirty) document.location.reload();
}());
