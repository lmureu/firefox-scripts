// ==UserScript==
// @name           fix facebook chat
// @version        0.5.2
// @author         Lorenzo Mureu
// @description    Removes Facebook margin in chat
// @include        *://www.facebook.com/*
// @run-at         document-end
// @namespace      tag:mureulor@gmail.com,2013-08:firefox-scripts/facebookchatfix.user.js
// @downloadURL    https://github.com/mooow/firefox-scripts/raw/master/facebookchatfix.user.js
// @grant          GM_addStyle
// ==/UserScript==

console.log("adding style4");
GM_addStyle( "\
span._51z7{\
	font-size-adjust: 0.35 !important;\
}\
div#fbDockChatBuddylistNub:not(.openToggler){\
  min-width: 0px !important;\
}\
div#fbDockChatBuddylistNub.openToggler {\
  min-width: 20vw !important;\
}\
div#fbDockChatBuddylistNub{\
  position:fixed !important;\
  left: 0px !important;\
}\
div#leftCol{\
  visibility:hidden;\
  width:0px;\
");
console.log("style added5");

console.log("gm1");
gm=document.getElementById('globalMargin');
if(gm){
	gm.style.margin="0px";
	console.log("done1");
}

console.log("gm3");
cc=document.getElementById("contentCol");
if(cc){
	cc.style.marginLeft="0px";
	console.log("done3");
}

function toggle_leftCol()
{
    console.log("lc2");
    lc=document.getElementById('leftCol');
    a=document.getElementById('fixfacebookchat_lctoggler');
    
    if(lc){
	    if ( lc.style.visibility=="visible" ) {
		lc.style.visibility="hidden";
		lc.style.width="0px";
		a.text="+";
	    } else {
		lc.style.visibility="visible";
		lc.style.width="20vw";
		a.text="-";
	    }
	    console.log("done2");
    }
}

a=document.createElement("a");
a.text="+";
a.id='fixfacebookchat_lctoggler';
a.addEventListener('click', toggle_leftCol, true);
a.style.position="fixed";
a.style.top="0";
a.style.left="0";
a.style.zIndex="10000000";
a.style.color="white";
document.body.appendChild(a);
