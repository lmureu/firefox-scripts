// ==UserScript==
// @name 			    Remove Wikipedia Funding ads
// @namespace 			tag:mureulor@gmail.com,2013-08:firefox-scripts/remove_wikipedia_funding_ads.user.js
// @downloadURL 		https://github.com/mooow/firefox-scripts/raw/master/remove_wikipedia_funding_ads.user.js
// @version 			0.1
// @description 		Removes funding ads' banners from wikipedia pages.
// @author 			    Lorenzo Mureu
// @include 			*://*.wikipedia.org/*
// @run-at			    document-end
// @grant 			    none
// ==/UserScript==

var toRemove = document.getElementsByClassName('frb');
var size = toRemove.length;
while(toRemove.length > 0) {
    toRemove[0].remove();
}
console.log("Removed " + size + " elements! :)");

/*
    Copyright (C) 2018 Lorenzo Mureu <mureulor@gmail.com>
    See LICENSE file.
 */